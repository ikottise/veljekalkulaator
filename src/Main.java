import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import java.text.DecimalFormat;
import static javafx.scene.paint.Color.WHITE;

public class Main extends Application {

    public Parent programmiSisu() {
        BorderPane sisu = new BorderPane();
        GridPane tabel = new GridPane();
        Pane ratas = new Pane();
        sisu.setLeft(tabel);
        sisu.setRight(ratas);

        /**
         * Nupud, mis mõjutavad programmi tööd
         */
        Button arvuta = new Button("Arvuta");
        Button exit = new Button("Välju");

        HBox vanad = new HBox();
        vanad.setSpacing(2);

        HBox nupud = new HBox();
        nupud.setSpacing(5);
        nupud.getChildren().addAll(arvuta, exit);

        Label hetkeAndmed = new Label("Hetkelised andmed: ");
        hetkeAndmed.setPrefSize(180, 10);
        hetkeAndmed.setTextFill(WHITE);

        /**
         * Siit algavad sisestuskastid, esimesena vanad mõõtmed.
         */
        TextField vanaVeljeLaiusKast = new TextField();
        vanaVeljeLaiusKast.setPromptText("Velje Laius");

        TextField vanaVeljeRaadiusKast = new TextField();
        vanaVeljeRaadiusKast.setPromptText("Velje Raadius");

        TextField vanaVeljeOffsetKast = new TextField();
        vanaVeljeOffsetKast.setPromptText("Velje Offset");

        TextField vanaRehviLaiusKast = new TextField();
        vanaRehviLaiusKast.setPromptText("Rehvi Laius");

        TextField vanaRehviKorgusKast = new TextField();
        vanaRehviKorgusKast.setPromptText("Rehvi Kõrgus");

        //lisan tekstiväljad esimesse ritta.
        vanad.getChildren().addAll(hetkeAndmed, vanaVeljeRaadiusKast, vanaVeljeLaiusKast, vanaVeljeOffsetKast, vanaRehviLaiusKast, vanaRehviKorgusKast);

        /**
         * Uued mõõtmed
         */
        HBox uued = new HBox();
        uued.setSpacing(2);
        Label uuedAndmed = new Label("Uued andmed: ");
        uuedAndmed.setPrefSize(180, 10);
        uuedAndmed.setTextFill(WHITE);

        TextField uusVeljeLaiusKast = new TextField();
        uusVeljeLaiusKast.setPromptText("Velje Laius");

        TextField uusVeljeRaadiusKast = new TextField();
        uusVeljeRaadiusKast.setPromptText("Velje Raadius");

        TextField uusVeljeOffsetKast = new TextField();
        uusVeljeOffsetKast.setPromptText("Velje Offset");

        TextField uusRehviLaiusKast = new TextField();
        uusRehviLaiusKast.setPromptText("Rehvi Laius");

        TextField uusRehviKorgusKast = new TextField();
        uusRehviKorgusKast.setPromptText("Rehvi Kõrgus");

        //lisan tekstiväljad teise ritta.
        uued.getChildren().addAll(uuedAndmed, uusVeljeRaadiusKast, uusVeljeLaiusKast, uusVeljeOffsetKast, uusRehviLaiusKast, uusRehviKorgusKast);

        VBox vBox = new VBox(10);
        vBox.getChildren().addAll(
                vanad,
                uued,
                nupud);
        sisu.setTop(vBox);


        /**
         * Siit tuleb tabel, mis näitab võrdlusi
         */
        Label hetkeAndmedTabel = new Label("Hetkelised andmed: ");
        hetkeAndmedTabel.setPrefSize(180, 10);
        hetkeAndmedTabel.setTextFill(WHITE);

        Label uuedAndmedTabel = new Label("Uued andmed: ");
        uuedAndmedTabel.setPrefSize(180, 10);
        uuedAndmedTabel.setTextFill(WHITE);

        Label diameeter = new Label("Diameeter: ");
        diameeter.setPrefSize(150, 20);
        diameeter.setTextFill(WHITE);

        Label ringjoon = new Label("Ringjoone pikkus: ");
        ringjoon.setPrefSize(150, 20);
        ringjoon.setTextFill(WHITE);

        Label v2ljaUlatavus = new Label("Väljaulatavus: ");
        v2ljaUlatavus.setPrefSize(150, 20);
        v2ljaUlatavus.setTextFill(WHITE);

        Label siseOsa = new Label("Siseosa: ");
        siseOsa.setPrefSize(150, 20);
        siseOsa.setTextFill(WHITE);

        Label spidomeetriViga = new Label("Spidomeetri viga: ");
        spidomeetriViga.setPrefSize(150, 20);
        spidomeetriViga.setTextFill(WHITE);

        Label viga90 = new Label("Viga 90 km/h: ");
        viga90.setPrefSize(150, 20);
        viga90.setTextFill(WHITE);

        Label k6rguseMuut = new Label("Sõidukõrguse muutus: ");
        k6rguseMuut.setPrefSize(180, 20);
        k6rguseMuut.setTextFill(WHITE);

        /**
         * Lisame tabelisse nimed enda kohale
         */
        tabel.setVgap(10);
        tabel.add(hetkeAndmedTabel, 2, 1);
        tabel.add(uuedAndmedTabel, 3, 1);
        tabel.add(diameeter, 1, 2);
        tabel.add(ringjoon, 1, 3);
        tabel.add(v2ljaUlatavus, 1, 4);
        tabel.add(siseOsa, 1, 5);
        tabel.add(spidomeetriViga, 1, 6);
        tabel.add(viga90, 1, 7);
        tabel.add(k6rguseMuut, 1, 8);
        exit.setOnAction(event -> System.exit(0));

        /**
         * Siin on kõik, mis toimub peale nupu "arvuta" vajutamist: meetodid arvutavad väärtusi
         * ja lisavad need tabelisse.
         */

        arvuta.setOnAction(event -> {
            double vanaVeljeRaadiusDouble = Double.parseDouble(vanaVeljeRaadiusKast.getText());
            double vanaVeljeLaiusDouble = Double.parseDouble(vanaVeljeLaiusKast.getText());
            double vanaVeljeOffsetDouble = Double.parseDouble(vanaVeljeOffsetKast.getText());
            double vanaRehviLaiusDouble = Double.parseDouble(vanaRehviLaiusKast.getText());
            double vanaRehviKorgusDouble = Double.parseDouble(vanaRehviKorgusKast.getText());

            double uusVeljeRaadiusDouble = Double.parseDouble(uusVeljeRaadiusKast.getText());
            double uusVeljeLaiusDouble = Double.parseDouble(uusVeljeLaiusKast.getText());
            double uusVeljeOffsetDouble = Double.parseDouble(uusVeljeOffsetKast.getText());
            double uusRehviLaiusDouble = Double.parseDouble(uusRehviLaiusKast.getText());
            double uusRehviKorgusDouble = Double.parseDouble(uusRehviKorgusKast.getText());

            //Ümardab double väärtused kaks kohta peale koma.
            DecimalFormat df = new DecimalFormat("#.##"); //alustan ümardamist

            /**
             * Vana ratta diameeter
             */
            Label vanaRattaDiameeter = new Label();
            double vanaRattaDiameeterDouble = Arvutaja.rattaDiameeter(vanaRehviKorgusDouble, vanaVeljeRaadiusDouble, vanaRehviLaiusDouble);
            String vanaRattaDiameeterDoubleRounded = df.format(vanaRattaDiameeterDouble);
            vanaRattaDiameeter.setText(vanaRattaDiameeterDoubleRounded + " mm");
            tabel.add(vanaRattaDiameeter, 2, 2);

            /**
             * Uue ratta diameeter
             */
            Label uusRattaDiameeter = new Label();
            double uusRattaDiameeterDouble = Arvutaja.rattaDiameeter(uusRehviKorgusDouble, uusVeljeRaadiusDouble, uusRehviLaiusDouble);
            String uusRattaDiameeterDoubleRounded = df.format(uusRattaDiameeterDouble);
            uusRattaDiameeter.setText(uusRattaDiameeterDoubleRounded + " mm");
            tabel.add(uusRattaDiameeter, 3, 2);

            /**
             * Vana ratta ringjoon
             */
            Label vanaRattaRingjoon = new Label();
            double vanaRattaRingjoonDouble = Arvutaja.rattaKeerlemisePikkus(vanaRattaDiameeterDouble);
            String vanaRattaRingjoonDoubleRounded = df.format(vanaRattaRingjoonDouble);
            vanaRattaRingjoon.setText(vanaRattaRingjoonDoubleRounded + " mm");
            tabel.add(vanaRattaRingjoon, 2, 3);

            /**
             * Uue ratta ringjoon
             */
            Label uusRattaRingjoon = new Label();
            double uusRattaRingjoonDouble = Arvutaja.rattaKeerlemisePikkus(uusRattaDiameeterDouble);
            String uusRattaRingjoonDoubleRounded = df.format(uusRattaRingjoonDouble);
            uusRattaRingjoon.setText(uusRattaRingjoonDoubleRounded + " mm");
            tabel.add(uusRattaRingjoon, 3, 3);

            /**
             * Vana ratta "poke"
             */
            Label vanaV2ljaUlatavus = new Label();
            double vanaV2ljaUlatavusDouble = Arvutaja.rattaV2ljaUlatavus(vanaVeljeOffsetDouble, vanaVeljeLaiusDouble);
            String vanaV2ljaUlatavusDoubleRounded = df.format(vanaV2ljaUlatavusDouble);
            vanaV2ljaUlatavus.setText(vanaV2ljaUlatavusDoubleRounded + " mm");
            tabel.add(vanaV2ljaUlatavus, 2, 4);

            /**
             * Uue ratta "poke"
             */
            Label uusV2ljaUlatavus = new Label();
            double uusV2ljaUlatavusDouble = Arvutaja.rattaV2ljaUlatavus(uusVeljeOffsetDouble, uusVeljeLaiusDouble);
            String uusV2ljaUlatavusDoubleRounded = df.format(uusV2ljaUlatavusDouble);
            uusV2ljaUlatavus.setText(uusV2ljaUlatavusDoubleRounded + " mm");
            tabel.add(uusV2ljaUlatavus, 3, 4);

            /**
             * Vana ratta siseosa
             */
            Label vanaRattaSiseosa = new Label();
            double vanaRattaSiseosaDouble = Arvutaja.rattaSiseOsa(vanaVeljeOffsetDouble, vanaVeljeLaiusDouble);
            String vanaRattaSiseosaDoubleRounded = df.format(vanaRattaSiseosaDouble);
            vanaRattaSiseosa.setText(vanaRattaSiseosaDoubleRounded + " mm");
            tabel.add(vanaRattaSiseosa, 2, 5);

            /**
             * Uue ratta siseosa
             */
            Label uusRattaSiseosa = new Label();
            double uusRattaSiseosaDouble = Arvutaja.rattaSiseOsa(uusVeljeOffsetDouble, uusVeljeLaiusDouble);
            String uusRattaSiseosaDoubleRounded = df.format(uusRattaSiseosaDouble);
            uusRattaSiseosa.setText(uusRattaSiseosaDoubleRounded + " mm");
            tabel.add(uusRattaSiseosa, 3, 5);

            /**
             * Spidomeetri viga
             */
            Label vanaSpidomeetriviga = new Label("0%"); // 0 sest võrreldakse hetkelise veaga.
            tabel.add(vanaSpidomeetriviga, 2, 6);

            /**
             * Uue ratta spidomeetri vea protsent
             */
            Label uusViga = new Label();
            double uusVigaDouble = Arvutaja.spidomeetriVeaProtsent(vanaRattaDiameeterDouble, uusRattaDiameeterDouble);
            String uusVigaRounded = df.format(uusVigaDouble);
            uusViga.setText(uusVigaRounded + " %");
            tabel.add(uusViga, 3, 6);

            /**
             * Spidomeetri viga 90km/h
             */
            Label viga50null = new Label("90 km/h");
            tabel.add(viga50null, 2, 7);

            Label viga90Label = new Label();
            double viga90Double = Arvutaja.spidomeetriViga(uusVigaDouble, 90);
            String viga90DoubleRounded = df.format(viga90Double);
            viga90Label.setText(viga90DoubleRounded + " km/h");
            tabel.add(viga90Label, 3, 7);

            /**
             * Sõidukõrguse muut
             */
            Label korguse0Muut = new Label("0 mm");
            tabel.add(korguse0Muut, 2, 8);

            Label korguseMuut = new Label();
            double korguseMuutDouble = Arvutaja.s6iduK6rguseMuutus(vanaRattaDiameeterDouble, uusRattaDiameeterDouble);
            String korguseMuutDoubleRounded = df.format(korguseMuutDouble);
            korguseMuut.setText(korguseMuutDoubleRounded + " mm");
            tabel.add(korguseMuut, 3, 8);

        });
        return sisu;
    }

    /**
     * "@Override on märge, mis ütleb, et käesolev meetod on super klassist (Application) üle kirjutatud."
     * võetud aadressilt http://i200.itcollege.ee/#JavaFX
     */
    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(programmiSisu(), 1000, 600);
        scene.getStylesheets().add("Main.css");
        scene.setFill(Paint.valueOf("#2980B9"));
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Ratta Kalkulaator");
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
