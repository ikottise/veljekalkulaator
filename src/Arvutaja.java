
public class Arvutaja {

    /**
     * Meetod, mis kasutab rehvi kõrgust, velje raadiust ja rehvi laiust,
     * et arvutada ratta välisdiameetri
     */
    public static double rattaDiameeter(double rehviKorgus, double veljeRaadius, double rehviLaius) {
        double diameeter = (2 * (rehviKorgus / 100 * rehviLaius)) + (veljeRaadius * 25.4);
        return diameeter;
    }

    /**
     * Meetod, mis arvutab ratta diameetri abil, kui pika tee
     * ratas ühe tiiruga läbib. Tuleb kasuks spidomeetri vea arvestamisel
     */
    public static double rattaKeerlemisePikkus(double rattaDiameeter) {
        double ringjoonePikkus = 3.14 * rattaDiameeter;
        return ringjoonePikkus;
    }

    /**
     * Meetod arvutab ratta väljaulatuva osa pikkuse.
     */
    public static double rattaV2ljaUlatavus(double veljeOffset, double veljeLaius) {
        double poke =  veljeLaius *  25.4 / 2 - veljeOffset;
        return poke;
    }

    /**
     * Arvutab velje tallast sisse poole ulatuva osa. Tuleb hiljem kasuks, et näha,
     * kui palju ulatab velg sissepoole.
     */
    public static double rattaSiseOsa (double veljeOffset, double veljeLaius) {
        double sisemineOsa = veljeLaius * 25.4 / 2 + veljeOffset;
        return sisemineOsa;
    }

    /**
     * Leiab üldise spidomeetri vea koefitsendi, et seda hiljem kasutada vea leidmiseks.
     */
    public static double spidomeetriVeaProtsent (double vanaRattaDiameeter, double uusRattaDiameeter) {
        double viga = (1 - (vanaRattaDiameeter / uusRattaDiameeter)) * 100;
        return viga;
    }

    /**
     * Arvutab välja, kui palju erineb spidomeetri näit eelmise ratta omast.
     */
    public static double spidomeetriViga (double spidomeetriVeaProtsent, double kiirus) {
        double viga = kiirus - (kiirus * spidomeetriVeaProtsent * 0.01);
        return viga;
    }

    /**
     * Meetod, mis arvutab sõiduki kõrguse tõusu või languse.
     */
    public static double s6iduK6rguseMuutus (double vanaRattaDiameeter, double uusRattaDiameeter) {
        double k6rguseMuut = Math.abs((uusRattaDiameeter - vanaRattaDiameeter) / 2);
        return k6rguseMuut;

    }

}
